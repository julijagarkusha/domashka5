// Написать две функции:
//
// averageStudentMark - выведет средний бал студента, ид которого передан в аргументе
//
// averageGroupMark - выведет средний бал по всем студентам. Средний балл группы равен
// сумме средних баллов каждого студента поделить на количество студентов.

const students = [
    {
        id: 10,
        name: 'John Smith',
        marks: [10, 8, 6, 9, 8, 7]
    },
    {
        id: 11,
        name: 'John Doe',
        marks: [ 9, 8, 7, 6, 7]
    },
    {
        id: 12,
        name: 'Thomas Anderson',
        marks: [6, 7, 10, 8]
    },
    {
        id: 13,
        name: 'Jean-Baptiste Emanuel Zorg',
        marks: [10, 9, 8, 9]
    }
]

const averageStudentMark = (studentId) => {
    const student = students.find(student => student.id === studentId);
    return student.marks.reduce((acc, item) => acc + item) / student.marks.length;
}

console.log('averageStudentMark : ', averageStudentMark(11));

const averageGroupMark = () => {
    let sum = 0;
    let count = 0;

    students.forEach(item => {
        count += item.marks.length;
        sum += item.marks.reduce((acc, item) => acc + item);
    }, sum);

    return sum / count;
}

console.log('debug averageGroupMark: ', averageGroupMark());
